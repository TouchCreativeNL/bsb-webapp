/** @type {import('next-sitemap').IConfig} */
module.exports = {
  siteUrl: process.env.SITE_URL || "https://bsb-training.com", // Vervang dit door de daadwerkelijke URL van je website
  changefreq: "daily",
  priority: 0.7,
  sitemapSize: 5000,
  generateRobotsTxt: true,
  exclude: [
    "/how-we-do-it/consultancy",
    "/how-we-do-it/research",
    "/how-we-do-it/insights",
    "/how-we-do-it/socials",
  ],

  // postbuild: "next-sitemap --config next-sitemap.config.js",
  // Optionele instellingen
  // priority: 0.5, // Stel de standaard prioriteit in (tussen 0 en 1)
  // changefreq: "daily", // Stel de standaard changefreq in ("always", "hourly", "daily", "weekly", "monthly", "yearly", "never")
  // sitemapSize: 5000, // Het maximale aantal URL's per sitemap-bestand (standaard is 50000)
  // generateSitemap.xml: true, // Genereer het sitemap.xml-bestand
  // generateRobotsTxt: true, // Genereer het robots.txt-bestand

  // Aangepaste instellingen voor specifieke pagina's (optioneel)
  // pagesConfig: [
  //   {
  //     priority: 1, // Aangepaste prioriteit voor deze pagina
  //     changefreq: "daily", // Aangepaste changefreq voor deze pagina
  //     lastmod: "2023-01-01", // Aangepaste laatst gewijzigd datum voor deze pagina
  //   },
  // ],

  // Extra functies om aangepaste URL's te genereren (optioneel)
  // additionalPaths: async (pages) => {
  //   // Voeg extra URL's toe op basis van specifieke logica
  //   return [
  //     {
  //       url: "/extra-page", // De URL van de extra pagina
  //       priority: 0.7, // Prioriteit voor deze extra pagina
  //       changefreq: "weekly", // Changefreq voor deze extra pagina
  //     },
  //   ];
  // },
};
