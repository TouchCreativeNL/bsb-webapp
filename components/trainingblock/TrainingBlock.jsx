import Image from 'next/image';
import RedirectLink from '../redirect-link/RedirectLink';
import { FormattedMessage } from 'react-intl';
export default function TrainingBlock({ data }) {
  const itemList = data;

  return (
    <div className='training-block'>
      <div className='trainings'>
        {itemList.map((item, index) => {
          return (
            <div key={index} className='training dark-image'>
              <Image
                src={'http:' + item.bannerImage.fields.file.url}
                width={250}
                height={330}
                layout='responsive'
                objectFit='cover'
                priority='true'
              />
              <div className='content'>
                <div className='title white'>
                  <h3>
                    <FormattedMessage id={`${item.headTitle}`} />{' '}
                  </h3>
                </div>

                <div className='paragraph'>
                  <p>
                    <FormattedMessage id={`${item.prefaceParagraph}`} />
                  </p>
                </div>

                <RedirectLink
                  id='discover-btn'
                  link={'trainings/' + item.slug}
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
