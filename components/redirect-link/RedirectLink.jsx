import Link from 'next/link'
import { FormattedMessage } from 'react-intl'

export default function RedirectLink({ text, link, id }){
    return(
        <Link href={ link }>
            <div className="redirect-link">
                <div className="text">
                    {id ? <FormattedMessage id={id}/> :  text }
                </div>
                <div className="icon">
                    <img
                        src="/images/right-chevron.svg"
                        alt="right-chevron"
                        width="20px"
                        height="20px"
                    />
                </div>
            </div>
        </Link>
    )

}