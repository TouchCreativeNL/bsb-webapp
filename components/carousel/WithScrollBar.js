import React, { useRef } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import RedirectLink from '../redirect-link/RedirectLink';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

class WithScrollbar extends React.Component {
  state = { additionalTransfrom: 0, offSetCarousel: 0 };

  render() {
    const content = this.props.data;
    const btnText = this.props.buttonText;
    const CustomSlider = ({ carouselState }) => {
      let value = 0;
      let carouselItemWidth = 0;
      if (this.Carousel) {
        carouselItemWidth = this.Carousel.state.itemWidth;
        const maxTranslateX = Math.round(
          // so that we don't over-slide
          carouselItemWidth *
            (this.Carousel.state.totalItems -
              this.Carousel.state.slidesToShow) +
            150
        );
        value = maxTranslateX / 100; // calculate the unit of transform for the slider
      }
      const { transform } = carouselState;
      return (
        <div className='custom-slider'>
          <input
            type='range'
            value={Math.round(Math.abs(transform) / value)}
            defaultValue={0}
            max={
              (carouselItemWidth *
                (carouselState.totalItems - carouselState.slidesToShow) +
                (this.state.additionalTransfrom === 150 ? 0 : 150)) /
              value
            }
            onChange={(e) => {
              if (this.Carousel.isAnimationAllowed) {
                this.Carousel.isAnimationAllowed = false;
              }
              const nextTransform = e.target.value * value;
              const nextSlide = Math.round(nextTransform / carouselItemWidth);
              if (
                e.target.value == 0 &&
                this.state.additionalTransfrom === 150
              ) {
                this.Carousel.isAnimationAllowed = true;
                this.setState({ additionalTransfrom: 0 });
              }
              this.Carousel.setState({
                transform: -nextTransform, // padding 20px and 5 items.
                currentSlide: nextSlide,
              });
            }}
            className='custom-slider__input'
          />
        </div>
      );
    };
    return (
      <Carousel
        ssr={false}
        ref={(el) => (this.Carousel = el)}
        arrows={false}
        customButtonGroup={<CustomSlider />}
        itemClass='slider-image-item'
        responsive={responsive}
        containerClass='carousel-container-with-scrollbar'
        additionalTransfrom={-this.state.additionalTransfrom}
        beforeChange={(nextSlide) => {
          if (nextSlide !== 0 && this.state.additionalTransfrom !== 150) {
            this.setState({ additionalTransfrom: 150 });
          }
          if (nextSlide === 0 && this.state.additionalTransfrom === 150) {
            this.setState({ additionalTransfrom: 0 });
          }
        }}
      >
        {content.map((item, index) => {
          return (
            <div key={index} className='item image-container increase-size'>
              <div className='image'>
                <img
                  src={'https:' + item.bannerImage.fields.file.url}
                  alt={item.title}
                  objectposition='center'
                  priority='true'
                />
              </div>
              <div className='content white'>
                <div className='title'>
                  <h2>{item.headTitle}</h2>
                </div>
                <div className='paragraph'>{item.subTitle}</div>
                {item.eventUrl ? (
                  <RedirectLink
                    link={item.eventUrl}
                    id={this.props.btnid ? this.props.btnid : btnText}
                  />
                ) : (
                  <RedirectLink
                    link={`${
                      this.props.parentPath ? this.props.parentPath + '/' : ''
                    }${item.slug}`}
                    id={this.props.btnid ? this.props.btnid : btnText}
                  />
                )}
              </div>
            </div>
          );
        })}
      </Carousel>
    );
  }
}

export default WithScrollbar;
