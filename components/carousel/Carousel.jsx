import Image from 'next/image';
import Link from 'next/link';
import RedirectLink from '../redirect-link/RedirectLink';

export default function Carousel({ data }){

    const itemList = data;

    return(
        <div className="carousel">
            <div className="items">
                {itemList.map((item, index) => {
                    return(
                        <div key={index} className="item">
                            <Image
                                src={`/images/${item.image}`}
                                alt={ item.title }
                                layout="fill"
                                objectFit="cover"
                                priority="true"
                            />
                            <div className="content white">
                                <div className="title light">
                                    <h2>{ item.title }</h2>
                                </div>
                                <RedirectLink link={ item.link } text={ item.linktext }/>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
