import MailchimpSubscribe from 'react-mailchimp-subscribe';
import CustomForm from '../custom-form/CustomForm';

const MailchimpFormContainer = ({ locale }) => {
  const postURL = `https://hotmail.us9.list-manage.com/subscribe/post?u=86a46c564c081dfe2de04ec00&id=02016ff402`;

  return (
    <div className='mc__form-container'>
      <MailchimpSubscribe
        url={postURL}
        render={({ subscribe, status, message }) => (
          <CustomForm
            status={status}
            message={message}
            onValidated={(FormData) => subscribe(FormData)}
            locale={locale}
          />
        )}
      />
    </div>
  );
};

export default MailchimpFormContainer;
