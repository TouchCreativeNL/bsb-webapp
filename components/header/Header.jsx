import {useState, useRef, useEffect} from 'react';
import {motion} from 'framer-motion'
import Image from 'next/image';
import Navigation from '../navigation/Navigation';
import MenuIcon from '@material-ui/icons/Menu';
import Container from '@material-ui/core/Container';
import { Router, useRouter } from 'next/router';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Link from 'next/link';
import { useCookies } from 'react-cookie';

export default function Header() {

    const router = useRouter();
    const [curLang, setLang] = useState(router.locale)
    const [isOpen, setOpen] = useState(false)
    const [navOpen, setNavOpen] = useState(false)
    const [navBackground, setNavBackground] = useState(false);
    const [ cookie, setCookie ] = useCookies(['NEXT_LOCALE']);
    const { locale } = router;
    const navRef = useRef();
    const [coloredLogo, setColoredLogo] = useState(false);
    const [whiteLogo, setWhiteLogo ] = useState();

    navRef.current = navBackground;

    useEffect(() => {
        setNavOpen(false);
        setOpen(false);
        if(router.pathname != '/'){
            setColoredLogo(true);
            setWhiteLogo(false);
        } else {
            setColoredLogo(false);
            setWhiteLogo(true);
        }
    },[router.pathname])

    useEffect(() => {
        const handleScroll = () => {
          const show = window.scrollY > 10
          setNavOpen(false);
          if (navRef.current !== show) {
            setNavBackground(show)
            setColoredLogo(show);
          }
        }

        document.addEventListener('scroll', handleScroll)
        return () => {
            document.removeEventListener('scroll', handleScroll);
        }


      }, [])

      const switchLanguage = (lang) => {
        const locale = lang;
        router.push('/','/', { locale, scroll: false });
        setOpen(!isOpen)
        if(cookie.NEXT_LOCALE !== locale){
          setCookie("NEXT_LOCALE", locale, { path: "/" });
          setLang(lang);
        }
      }

    return(
        <div className={`header-wrap ${navBackground ? `white` : ``}`}>
            <Container  maxWidth="md">
            <div className="header-inner">
                <div className="header-bar">
                    <div className={`logo ${coloredLogo || navOpen || !whiteLogo ? `colored` : `white`}`}>
                        <Link href="/">
                        <Image
                            class="logo"
                            src={`/images/${coloredLogo || router.pathname != '/' || navOpen ? `colored-logo.png` : `white-logo.png` }`}
                            alt="bsb-logo"
                            layout="fill"
                            objectFit="contain"
                        />
                        </Link>
                    </div>
                    <div className="right-side">
                        <div className={`language-wrap ${ router.pathname != '/' || navOpen || navBackground ? `dark` : ``}`}>
                        <div className="cur-lang">
                                { curLang }
                            </div>
                            <motion.div className="icon" onClick={() => setOpen(!isOpen)}
                                  animate={{
                                    rotate: isOpen ? 180 : 0
                                }}

                            >
                                <KeyboardArrowDownIcon/>
                            </motion.div>
                            <motion.div
                                animate={{
                                    opacity: isOpen ? 1.0 : 0
                                }}
                            >
                                <ul className="lang-list">
                                    <li onClick={() => switchLanguage('nl') }>NL</li>
                                    <li onClick={() => switchLanguage('en') }>EN</li>
                                </ul>
                            </motion.div>
                        </div>
                        <div onClick={() => setNavOpen(!navOpen)} className="hamburger-icon">
                                <MenuIcon fontSize="small"/>
                            </div>
                    </div>
                </div>
                </div>
            </Container>
            <Navigation open={navOpen}/>
        </div>
    )
}
