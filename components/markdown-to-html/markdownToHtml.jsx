import DOMPurify from "dompurify";
import { marked } from "marked";
import React, { useEffect, useState } from "react";

export const MarkdownToHtml = ({ content }) => {
  const rawContent = content;
  //   const sanitizedContent = DOMPurify.sanitize(markdownContent);
  const [sanitizedHtmlContent, setSanitizedHtmlContent] = useState("");

  useEffect(() => {
    if (typeof window !== "undefined") {
      const markdownContent = marked(rawContent);
      const sanitizedContent = DOMPurify.sanitize(markdownContent);
      setSanitizedHtmlContent(sanitizedContent);
    }
  }, [rawContent]);
  return (
    <>
      <div dangerouslySetInnerHTML={{ __html: sanitizedHtmlContent }}></div>
    </>
  );
};
