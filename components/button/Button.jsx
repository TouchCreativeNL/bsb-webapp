import Link from 'next/link';
import Image from 'next/image';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { FormattedMessage } from 'react-intl';

export default function Button({
  text,
  link,
  nofill,
  id,
  disabled,
  target = '_blank',
}) {
  return (
    <a target={target} href={link}>
      <div
        className={`button ${nofill ? `nofill` : ``} ${
          disabled ? 'disabled' : ''
        } `}
      >
        <div className='text white'>
          {id ? <FormattedMessage id={id} /> : text}
        </div>
        <div className='icon'>
          <ChevronRightIcon fontSize='large' />
        </div>
      </div>
    </a>
  );
}
