import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Router from "next/router";

const convertBreadcrumb = (string) => {
  return string.replace(/-/g, " ").replace(/oe/g, "ö").replace(/ae/g, "ä").replace(/ue/g, "ü");
};

const Breadcrumbs = () => {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState(null);

  useEffect(() => {
    if (router) {
      const linkPath = router.asPath.split("/");
      linkPath.shift();

      const pathArray = linkPath.map((path, i) => {
        return { breadcrumb: path, href: "/" + linkPath.slice(0, i + 1).join("/") };
      });

      setBreadcrumbs(pathArray);
    }
  }, [router]);

  if (!breadcrumbs) {
    return null;
  }

  if (breadcrumbs[0].breadcrumb === "this-is-us") {
    breadcrumbs[0].breadcrumb = "who-we-are";
  }

  if (breadcrumbs[0].breadcrumb === "why-we-do-it") {
    breadcrumbs[0].breadcrumb = "What-we-can-do-for-you";
  }

  return (
    <div className="breadcrumbs">
      <div className="icon breadcrumb_icon" onClick={() => Router.back()}>
        <img src="/images/right-chevron.svg" alt="right-chevron" width="15px" height="15px" />
      </div>
      <ul className="list">
        <li className="home">
          <a href="/">Home</a>
          <div className="slash">/</div>
        </li>
        {breadcrumbs.map((breadcrumb, i) => {
          return (
            <li key={breadcrumb.href}>
              <Link href={breadcrumb.href}>
                <a>{convertBreadcrumb(breadcrumb.breadcrumb)}</a>
              </Link>
              <div className="slash">/</div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Breadcrumbs;
