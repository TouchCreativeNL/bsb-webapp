import { useState, useEffect } from 'react';
import Image from 'next/image';
import Popup from '../popup/popup';

export default function BannerWhitepaper({ title, background, style, locale }) {
  const [isOpen, toggleIsOpen] = useState(false);

  useEffect(() => {
    if (isOpen) document.body.style.overflow = 'hidden';
    if (!isOpen) document.body.style.overflow = 'scroll';
  }, [isOpen]);

  return (
    <div className='newbanner'>
      <div className='background-banner'>
        <div className={style}>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels copy 8.png`}
            width={800}
            height={800}
            priority='true'
          />
        </div>
        <Image
          src={`/images/${background}`}
          layout='fill'
          objectFit='cover'
          loading='lazy'
        />
      </div>
      <div className='content'>
        <div className='title light'>
          <h2>{title}</h2>
        </div>
        <button
          className='button no-border'
          onClick={() => toggleIsOpen(!isOpen)}
        >
          <div className='text white'>Download</div>
          <div className='icon'>
            <Image src='/images/right-chevron.svg' width='15' height='15' />
          </div>
        </button>
      </div>

      {isOpen && (
        <Popup
          toggleIsOpen={toggleIsOpen}
          isOpen={isOpen}
          // checkText={checkboxText}
          locale={locale}
        />
      )}
    </div>
  );
}
