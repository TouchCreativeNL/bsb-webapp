import Image from "next/image";
import Button from "../button/Button";
import Link from "next/link";
import { FormattedMessage } from "react-intl";
import client from "../../client";
import { useRouter } from "next/router";

export default function Footer() {
  const { asPath } = useRouter();

  return (
    <div className="footer-wrap">
      <div className="footer">
        <div class="cirkels_footer">
          <Image class="cirkel" src={`/images/20751_BSB_logo-cirkels_vol.png`} width={350} height={350} priority="true" />
        </div>
        <div className="content">
          <div className="item">
            <div className="logo">
              <Image src="/images/logo-white.png" alt="bsb-logo" layout="fill" objectFit="contain" />
            </div>
            <div className="socials">
              <div className="icon">
                <a href="mailto:info@bsb-training.com">
                  <Image src="/images/mail.svg" alt="mail" width={25} height={25} objectFit="contain" />
                </a>
              </div>
              <div className="icon">
                <a href="https://www.linkedin.com/in/julia-brouwers-4a494aa5/">
                  <Image src="/images/linkedin.svg" alt="linkedin" width={25} height={25} objectFit="contain" />
                </a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="title light">
              <h2>
                <FormattedMessage id="footer-contact-us" />
              </h2>
            </div>
            <div className="paragraph light">
              <FormattedMessage id="footer-contact-us-para" />
              <br />
              <b>info@bsb-training.com</b>
            </div>
            <br />
            <Button id="contact-us" link="/contact-us" />
          </div>
          <div className="item">
            <div className="title light">
              <h2>Kasteel Heemstede</h2>
            </div>
            <div className="paragraph light">
              <p>
                Heemsteedseweg 26b
                <br />
                3992 LS
                <br />
                Houten
              </p>
            </div>
            <div className="phonenumber">
              <a href="tel:+31303042000">+31303042000</a>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bar">
        <div className="left">
          <div className="paragraph light">
            <p>
              <i>Copyright © 2010-2024 BSB-Training</i>
            </p>
          </div>
        </div>
        <div className="right">
          <div className="policys">
            <div className="policy">
              <Link href="/terms-and-conditions">
                <a>
                  <FormattedMessage id="terms" />
                </a>
              </Link>
            </div>
            <div className="policy">
              <Link href="/disclaimer">Disclaimer</Link>
            </div>
            <div className="policy">
              <Link href="/privacy">Privacy Statement</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
