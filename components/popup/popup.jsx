import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { MdClose } from 'react-icons/md';
import { FormattedMessage } from 'react-intl';
import MailchimpFormContainer from '../mailchimp-formcontainer/MailchimpFormContainer';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const Popup = ({ toggleIsOpen, isOpen, locale }) => {
  const [submitted, setSubmitted] = useState(false);

  const handleClose = e => {
    if (e.target === document.querySelector('.overlay')) {
      toggleIsOpen(false);
    }
  };

  return (
    <>
      <motion.div
        initial='hidden'
        animate='visible'
        variants={{
          hidden: {
            opacity: 0,
          },
          visible: {
            opacity: 1,
          },
        }}
      >
        <div className='overlay' onClick={handleClose}>
          <div className='popup'>
            {!submitted ? (
              <>
                <h2 className='title'>Download whitepaper</h2>

                <span onClick={() => toggleIsOpen(false)} className='close-btn'>
                  <MdClose className='close-icon' />
                </span>

                <p className='popup__text'>
                  <FormattedMessage id='popup' />
                </p>

                {/* <MailchimpFormContainer locale={locale} /> */}

                <form
                  method='POST'
                  action='https://behaviouralscienceforbusiness.activehosted.com/proc.php'
                  id='_form_3_'
                  className='_form _form_3 _inline-form  _dark'
                  noValidate
                  data-styles-version='3'
                >
                  <input type='hidden' name='u' value='3' />
                  <input type='hidden' name='f' value='3' />
                  <input type='hidden' name='s' />
                  <input type='hidden' name='c' value='0' />
                  <input type='hidden' name='m' value='0' />
                  <input type='hidden' name='act' value='sub' />
                  <input type='hidden' name='v' value='2' />
                  <input
                    type='hidden'
                    name='or'
                    value='9494dd3d71f50b896eaeb026a0b8350f'
                  />
                  <div className='_form-content popup__formcontent'>
                    <div className='_form_element _x07858933 _full_width pop__inputcontainer'>
                      <label htmlFor='email' className='_form-label'></label>
                      <div
                        className='_field-wrapper popup__inputwrapper'
                        style={{ display: 'flex', flexDirection: 'column' }}
                      >
                        <input
                          type='text'
                          id='email'
                          name='email'
                          placeholder='E-mail'
                          required
                          className='popup__input'
                        />
                      </div>
                    </div>
                    <div className='_button-wrapper _full_width'>
                      <button
                        id='_form_3_submit'
                        className='_submit popup__submit no-border'
                        type='submit'
                      >
                        Download whitepaper
                        <div className='popup__submit-icon'>
                          <ChevronRightIcon fontSize='' />
                        </div>
                      </button>
                    </div>
                    <div className='_clear-element'></div>
                  </div>

                  {/* <div className='_form-thank-you' style='display:none;'></div> */}
                </form>
              </>
            ) : (
              <>
                <h2 className='title'>
                  <FormattedMessage id='popup-thanks-title' />
                </h2>

                <p className='popup__text'>
                  <FormattedMessage id='popup-thanks-para' />
                </p>

                <button
                  className='popup__submit no-border'
                  onClick={() => toggleIsOpen(false)}
                >
                  <FormattedMessage id='popup-thanks-button' />
                </button>
              </>
            )}
          </div>
        </div>
      </motion.div>
    </>
  );
};

export default Popup;
