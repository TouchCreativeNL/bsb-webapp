import Link from "next/link";
import { motion } from "framer-motion";
import Container from "@material-ui/core/Container";
import { useRouter } from "next/router";
import { FormattedMessage } from "react-intl";

export default function Navigation({ open, closeNav }) {
  const router = useRouter();

  return (
    <motion.div
      className="navigation-wrap"
      initial={{ height: 0 }}
      animate={open ? { height: "100vh" } : { height: "0%" }}
      transition={{ duration: 0.25 }}
    >
      <Container maxWidth="md">
        <div className="navigation-content">
          <div className="navigation-items">
            <div className={`navigation-item ${router.pathname == "/" ? `active` : ``}`}>
              <Link href="/">
                <div>
                  <FormattedMessage id="nav-item-1" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/how-we-do-it" ? `active` : ``}`}>
              <Link href="/how-we-do-it">
                <div>
                  <FormattedMessage id="nav-item-2" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/trainings" ? `active` : ``}`}>
              <Link href="/trainings">
                <div>
                  <FormattedMessage id="nav-item-3" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/this-is-us" ? `active` : ``}`}>
              <Link href="/this-is-us">
                <div>
                  <FormattedMessage id="nav-item-4" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/our-thoughts" ? `active` : ``}`}>
              <Link href="/our-thoughts">
                <div>
                  <FormattedMessage id="nav-item-5" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/events" ? `active` : ``}`}>
              <Link href="/events">
                <div>
                  <FormattedMessage id="nav-item-6" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/why-we-do-it" ? `active` : ``}`}>
              <Link href="/why-we-do-it">
                <div>
                  <FormattedMessage id="nav-item-8" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            <div className={`navigation-item ${router.pathname == "/contact-us" ? `active` : ``}`}>
              <Link href="/contact-us">
                <div>
                  <FormattedMessage id="nav-item-7" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div>
            {/* Temporary removal of newsletter page, this will return in the future */}
            {/* <div className={`navigation-item ${router.pathname == "/newsletter" ? `active` : ``}`}>
              <Link href="/newsletter">
                <div>
                  <FormattedMessage id="nav-item-9" />
                </div>
              </Link>
              <div className="icon">
                <img src="/images/right-chevron.svg" alt="right-chevron" width="20px" height="20px" />
              </div>
            </div> */}
          </div>
        </div>
      </Container>
    </motion.div>
  );
}
