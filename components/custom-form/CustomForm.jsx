import { useState, useEffect } from 'react';
import InputField from '../inputfield/InputField';
import Checkbox from '../checkbox/Checkbox';
import Loading from '../loading/Loading';

const CustomForm = ({ status, message, onValidated, locale }) => {
  const [email, setEmail] = useState('');
  const [isChecked, toggleIsChecked] = useState(false);

  useEffect(() => {
    if (status === 'success') clearFields();
  }, [status]);

  const clearFields = () => {
    setEmail('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    email &&
      email.indexOf('@') > -1 &&
      onValidated({
        EMAIL: email,
      });
  };

  return status === 'sending' ? (
    <div className='mc__alert mc__alert--sending'>
      <Loading />
    </div>
  ) : status === 'success' ? (
    <div
      className='mc__alert mc__alert--success'
      dangerouslySetInnerHTML={{ __html: message }}
    ></div>
  ) : (
    <form className='mc__form' onSubmit={(e) => handleSubmit(e)}>
      {/* <h3 className='mc__title'>
        {status === 'success' ? 'Success!' : 'Join our email list blabla'}
      </h3> */}

      {status === 'error' && (
        <div
          className='mc__alert mc__alert--error'
          dangerouslySetInnerHTML={{ __html: message }}
        ></div>
      )}

      <InputField
        label=''
        onChangeHandler={setEmail}
        type='email'
        value={email}
        placeholder='E-mail'
        isRequired
      />

      <Checkbox
        type='checkbox'
        label={
          locale == 'nl'
            ? 'Ik ga akkoord met de algemene voorwaarden'
            : 'I agree with the terms & conditions'
        }
        isChecked={isChecked}
        toggleIsChecked={toggleIsChecked}
        inputId='conditions'
        className='mc__input-conditions'
      />

      <InputField
        btnText='Download whitepaper'
        type='submit'
        formValues={[email, isChecked]}
      />
    </form>
  );
};

export default CustomForm;
