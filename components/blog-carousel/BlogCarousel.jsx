import Carousel from '../carousel/Carousel';
import { data as secondCarousel } from '../../public/data/second-carousel';

export default function BlogCarousel(){
    return(
        <div className="blog-carousel">
            <Carousel data={ secondCarousel }/>
        </div>
    )
}