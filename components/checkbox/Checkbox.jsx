import React from 'react';

const Checkbox = ({
  type,
  label,
  isChecked,
  toggleIsChecked,
  inputId,
  className,
}) => {
  return (
    <label className='mc__label-conditions' htmlFor={inputId}>
      <input
        type={type}
        className={className}
        id={inputId}
        checked={isChecked}
        onChange={() => toggleIsChecked(!isChecked)}
      />
      {label}
    </label>
  );
};

export default Checkbox;
