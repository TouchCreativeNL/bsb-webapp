import Image from 'next/image';
import { FormattedMessage } from 'react-intl';

export default function Banner({ data, title, background, style }) {
  const itemList = data;

  return (
    <div className='newbanner2'>
      <div className='background-banner'>
        <div className={style}>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels copy 8.png`}
            width={800}
            height={800}
            priority='true'
          />
        </div>
        <Image
          src={`/images/${background}`}
          layout='fill'
          objectFit='cover'
          loading='lazy'
        />
      </div>
      <div className='content'>
        <div className='title light'>
          <h2>{title}</h2>
        </div>
        <div className='items'>
          {itemList.map((item, index) => {
            return (
              <div className='item' key={index}>
                <div className='icon'>
                  <a target='_blank' href={`${item.link}`}>
                    <Image
                      src={`/images/${item.image}`}
                      alt={item.title}
                      layout='fill'
                      objectFit='contain'
                    />
                  </a>
                </div>
                <div className='text white'>{item.title}</div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
