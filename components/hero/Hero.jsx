import Image from "next/image";
import Link from "next/link";
import RedirectLink from "../redirect-link/RedirectLink";
import Container from "@material-ui/core/Container";
import { FormattedMessage } from "react-intl";

export default function Hero({ text }) {
  return (
    <div className="hero-wrap">
      <div className="hero-item">
        <video autoPlay loop muted>
          <source src={require("../../public/videos/bsb-hero-video.mp4")} />
        </video>
        <div className="textblock">
          <Container maxWidth="md">
            <div className="content">
              <div className="title black">
                <h1>
                  <FormattedMessage id={text.titleblockTitle} />
                </h1>
              </div>
              <div className="paragraph">
                <p>
                  <FormattedMessage id={text.titleBlockParagraph} />
                </p>
              </div>
              <RedirectLink link="/this-is-us" id="mt-consulting-agency-btn" />
            </div>
          </Container>
        </div>
      </div>
    </div>
  );
}
