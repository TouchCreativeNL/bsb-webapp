import Image from "next/image";
import RedirectLink from "../redirect-link/RedirectLink";

export default function EventsBlock({ data, text, path, buttontext }) {
  const itemList = data;
  const linkText = text;
  const linkPath = path;

  return (
    <div className="events-block">
      <div className="content">
        <div className="events">
          {itemList.map((item, key) => {
            return (
              <div key={key} className="event dark-image">
                <Image src={"https:" + item.bannerImage.fields.file.url} layout="fill" objectFit="cover" loading="lazy" />
                <div className="text-block">
                  <div className="title light">
                    <h2>{item.headTitle}</h2>
                    <h4>{item.headTitleDate}</h4>
                  </div>
                  <RedirectLink link={linkPath + "/events/" + item.slug} id={buttontext ? buttontext : "further-details-btn"} />
                </div>
              </div>
            );
          })}
        </div>
        {/* {linkText ? (
          <div className='all-events'>
            <RedirectLink link={linkPath} id={linkText} />
          </div>
        ) : (
          ''
        )} */}
      </div>
    </div>
  );
}
