import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const InputField = ({
  type,
  label,
  onChangeHandler,
  placeholder,
  value,
  isRequired,
  name,
  formValues,
  btnText,
}) => {
  const validateInput = (values) => {
    if (values[0].indexOf('@') === -1 || !values[1]) {
      return true;
    } else {
      return false;
    }
  };

  if (type === 'submit') {
    return (
      <button
        className={`
        submit button no-border ${validateInput(formValues) && 'disabled'}`}
        type='submit'
        disabled={validateInput(formValues)}
      >
        {btnText}
        <div className='icon'>
          <ChevronRightIcon fontSize='large' />
        </div>
      </button>
    );
  } else if (type === 'textarea') {
    return (
      <label className='label'>
        {label}
        <textarea
          onChange={(e) => onChangeHandler(e.target.value)}
          placeholder={placeholder}
          value={value}
          required={isRequired}
          className='textarea'
          rows={7}
          name={name}
        />
      </label>
    );
  } else {
    return (
      <input
        onChange={(e) => onChangeHandler(e.target.value)}
        type={type}
        placeholder={placeholder}
        value={value}
        required={isRequired}
        className='input'
        name={name}
      />
    );
  }
};

export default InputField;
