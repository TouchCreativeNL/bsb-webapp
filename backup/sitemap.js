import { getServerSideSitemap, getServerSideSitemapIndex } from "next-sitemap";
import fs from "fs";
import path from "path";

// Functie om dynamisch alle pagina's te vinden en hun gegevens te genereren
const generateSiteMapData = () => {
  const pagesDirectory = path.join(process.cwd(), "pages");
  const excludedFiles = ["_app.js", "_document.js", "_error.js", "sitemap.js"]; // Voeg bestandsnamen toe die je wilt uitsluiten

  const pages = fs
    .readdirSync(pagesDirectory)
    .filter((fileName) => !excludedFiles.includes(fileName))
    .map((fileName) => {
      const route = `/${fileName.replace(".js", "")}`; // Pagina-URL is gebaseerd op de bestandsnaam
      const lastmod = new Date().toISOString(); // Laatste wijzigingsdatum (hier wordt de huidige datum gebruikt)

      return { loc: route, lastmod };
    });

  return pages;
};

export const getServerSideProps = async (ctx) => {
  // Haal dynamisch de sitemap-gegevens op
  const data = generateSiteMapData();

  // Genereer de sitemap
  const sitemap = getServerSideSitemap(data);

  // Genereer de index-sitemap (optioneel)
  const sitemapIndex = getServerSideSitemapIndex({
    sitemapSize: 5, // Limiteer het aantal URL's per sitemap-bestand
    sitemapUrl: "/sitemap.xml", // De URL van de gegenereerde sitemap
  });

  ctx.res.setHeader("Content-Type", "application/xml");
  ctx.res.write(sitemap);
  ctx.res.end();

  // Als je ook een index-sitemap wilt genereren, kun je deze lijn toevoegen:
  // ctx.res.end(sitemapIndex)
};

export default function Sitemap() {
  // Deze component wordt niet gerenderd, maar de server-side sitemap wordt gegenereerd via getServerSideProps
  return null;
}
