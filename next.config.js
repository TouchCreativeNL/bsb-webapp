// next.config.js
const withVideos = require("next-videos");
const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([withVideos], {
  async redirects() {
    return [
      {
        source: "/events/event-1",
        destination: "/events/behavioural-strategy",
        permanent: true,
      },
      {
        source: "/events/event-2",
        destination: "/events/decision-making",
        permanent: true,
      },
      {
        source: "/events/event-3",
        destination: "/events/behavioural-finance",
        permanent: true,
      },
      {
        source: "/events/event-4",
        destination: "/events/human-dynamics",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-1",
        destination: "/our-thoughts/getting-published",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-2",
        destination: "/our-thoughts/always-learning",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-3",
        destination: "/our-thoughts/hbr-book",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-4",
        destination: "/our-thoughts/our-new-office-home",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-5",
        destination: "/our-thoughts/peak-end-effect",
        permanent: true,
      },
      {
        source: "/our-thoughts/blog-post-6",
        destination: "/our-thoughts/counter-group-think",
        permanent: true,
      },
      {
        source: "/newsletter",
        destination: "/",
        permanent: false,
      },
    ];
  },

  i18n: {
    locales: ["nl", "en"],
    defaultLocale: "en",
    localeDetection: false,
  },
  env: {
    SPACE_ID: "7sif7u4nrjfl",
    ACCESS_TOKEN: "G1vpHG1Zuz4wubEGgNhvnUBL2afAA7PfYnReNYEt-7I",
  },
  images: {
    domains: ["images.ctfassets.net"],
  },
});
