import Container from '@material-ui/core/Container';
import Image from 'next/image';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import WithScrollbar from '../components/carousel/WithScrollBar';
import Button from '../components/button/Button';
import { FormattedMessage } from 'react-intl';
import client from '../client';
import Head from 'next/head';

export const getServerSideProps = async ({ params, locale }) => {
  let dataThoughtsPage = [];
  let dataBooks = [];
  let dataBlogs = [];
  let dataLinks = [];
  let dataSpotify = [];

  const curLocale = locale == 'nl' ? locale : 'en-US';

  const { items } = await client.getEntries({
    content_type: 'books',
    content_type: 'spotifyItems',
    content_type: 'interestingLinks',
    content_type: 'onzeGedachten',
    locale: curLocale,
  });

  await client
    .getEntries({ content_type: 'onzeGedachten', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataThoughtsPage.push(item.fields);
      });
    });

  await client
    .getEntries({ content_type: 'books', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataBooks.push(item.fields);
      });
    });

  await client
    .getEntries({ content_type: 'interestingLinks', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataLinks.push(item.fields);
      });
    });

  await client
    .getEntries({ content_type: 'spotifyItems', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataSpotify.push(item.fields);
      });
    });

  await client
    .getEntries({ content_type: 'blogPost', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataBlogs.push(item.fields);
      });
    });

  return {
    props: {
      dataThoughtsPage,
      dataThoughtsPage: items[0].fields,
      dataBooks,
      dataLinks,
      dataBlogs,
      dataSpotify,
      curLocale,
    },
  };
};

export default function Home({
  dataThoughtsPage,
  dataBlogs,
  dataBooks,
  dataLinks,
  dataSpotify,
  curLocale,
}) {
  const language = curLocale;
  const blogFields = dataBlogs;
  const content = dataThoughtsPage;
  const itemList = dataBooks;
  const interestingItems = dataLinks;
  const spotifyItems = dataSpotify;

  return (
    <div className='content'>
      <Head>
        {/* <title>
          BSB-training | {language == 'nl' ? 'Onze gedachten' : 'Our thoughts'}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id={content.ourThoughtsTitle} />
                  </h1>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id={content.ourThoughtsParagraph} />
                  </p>
                </div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner'>
                <Image
                  src={'https:' + content.ourThoughtsImage.fields.file.url}
                  alt={content.ourThoughtsImageAlt}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='our-interests-wrap'>
        <div className='cirkel_left_wrap'>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels_groen.png`}
            width={150}
            height={1300}
            priority='true'
          />
        </div>
        <Container maxWidth='md'>
          <div className='title'>
            <h2>
              <FormattedMessage id='ot-oibl-1-title' />
            </h2>
          </div>
          <div className='our-interest-block'>
            <div className='left'>
              <div className='our-books'>
                <div className='title'>
                  <h3>
                    <FormattedMessage id='ot-oibl-1-sub-title' />
                  </h3>
                </div>
                <div className='booklist'>
                  {itemList.map((item, index) => {
                    return (
                      <div key={index} className='book'>
                        <div className='book-image'>
                          <Image
                            src={'https:' + item.image.fields.file.url}
                            layout='fill'
                            objectFit='cover'
                            objectPosition='center'
                            alt={item.imageAlt}
                          />
                        </div>
                        <div className='content'>
                          <div className='title'>
                            <h3>
                              <FormattedMessage id={item.title} />
                            </h3>
                          </div>
                          <div className='paragraph'>
                            <p>
                              <FormattedMessage id={item.paragraph} />
                            </p>
                          </div>
                          <Button nofill id='view-this-book' link={item.link} />
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className='right'>
              <div className='our-music'>
                <div className='title'>
                  <h3>
                    <FormattedMessage id='ot-oibl-2-sub-title' />
                  </h3>
                </div>
                <div className='podcast'>
                  {spotifyItems.map((item, index) => {
                    return (
                      <iframe
                        key={index}
                        src={item.embedLink}
                        width='100%'
                        height='152'
                        frameBorder='0'
                        allowtransparency='true'
                        allow='encrypted-media'
                      ></iframe>
                    );
                  })}
                </div>
              </div>
              <div className='our-links'>
                <div className='title'>
                  <h3>
                    <FormattedMessage id='ot-oibl-3-sub-title' />
                  </h3>
                </div>
                <div className='links'>
                  {interestingItems.map((item, index) => {
                    return (
                      <div key={index} className='link'>
                        <Button nofill text={item.title} link={item.link} />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='blog-posts-wrap mg-bot-100'>
        <Container maxWidth='md'>
          <div className='title'>
            <h2>
              <FormattedMessage id='our-thoughts-blogs-title' />
            </h2>
          </div>
        </Container>
        <div className='blog-posts text-underneath'>
          <WithScrollbar
            parentPath='our-thoughts'
            btnid={language == 'nl' ? 'Doorgaan met lezen' : 'Continue reading'}
            data={blogFields}
          />
        </div>
      </div>
    </div>
  );
}
