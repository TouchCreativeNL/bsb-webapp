import EventsBlock from '../components/eventsblock/Eventsblock';
import Container from '@material-ui/core/Container';
import Image from 'next/image';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';
import client from '../client';
import Head from 'next/head';

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == 'nl' ? locale : 'en-US';

  let dataHowPage = [];
  let dataSpecialties = [];

  const { items } = await client.getEntries({
    content_type: 'speciality',
    content_type: 'hoeDoenWeHet',
    locale: curLocale,
  });

  await client
    .getEntries({ content_type: 'hoeDoenWeHet', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataHowPage.push(item.fields);
      });
    });

  await client.getEntries({ content_type: 'speciality' }).then(entries => {
    const list = entries.items;
    list.map(item => {
      return dataSpecialties.push(item.fields);
    });
  });

  return {
    props: {
      dataHowPage,
      dataHowPage: items[0].fields,
      dataSpecialties,
      curLocale,
    },
  };
};

export default function Home({ dataHowPage, dataSpecialties, curLocale }) {
  const language = curLocale;
  const specialityFields = dataSpecialties;
  const content = dataHowPage;

  return (
    <div className='content'>
      <Head>
        {/* <title>
          BSB-training | {language == "nl" ? "Hoe we het doen" : "How we do it"}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id={content.titleBlockTitle} />
                  </h1>
                </div>
                <div className='paragraph preface mg-bot-25'>
                  <p>
                    <FormattedMessage id={content.titleBlockSubtitle} />
                  </p>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id={content.titleBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner dark-image'>
                <Image
                  class='cirkel_howwedo'
                  src={`/images/20751_BSB_logo-cirkels copy 8.png`}
                  width={2000}
                  height={2000}
                  priority='true'
                />
                <Image
                  src={'https:' + content.topImage.fields.file.url}
                  alt={content.topImageAlt}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='eventsblock-wrap mg-bot-50'>
        <Container maxWidth='md'>
          <div className='textblock-wrap'>
            <div className='cirkel_howwedo_wrap'>
              <Image
                class='cirkel'
                src={`/images/20751_BSB_logo-cirkels_groen.png`}
                width={150}
                height={1300}
                priority='true'
              />
            </div>
            <div className='textblock'>
              <div className='content'>
                <div className='title'>
                  <h2>
                    <FormattedMessage id={content.addedBlockTitle} />
                  </h2>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id={content.addedBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
