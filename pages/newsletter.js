import EventsBlock from "../components/eventsblock/Eventsblock";
import Container from "@material-ui/core/Container";
import Image from "next/image";
import Breadcrumbs from "../components/breadcrumbs/Breadcrumbs";
import { FormattedMessage } from "react-intl";
import client from "../client";
import Head from "next/head";
import image from "/public/images/newsletter.webp";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == "nl" ? locale : "en-US";

  let dataNewsletter = [];

  const { items } = await client.getEntries({
    content_type: "newsletter",
    locale: curLocale,
  });

  await client.getEntries({ content_type: "newsletter", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataNewsletter.push(item.fields);
    });
  });

  return {
    props: {
      dataNewsletter,
      dataNewsletter: items[0].fields,
      curLocale,
    },
  };
};

export default function Home({ dataNewsletter, curLocale }) {
  const language = curLocale;
  const content = dataNewsletter;

  console.log(content);

  return (
    <div className="content">
      <Head>
        {/* <title>
          BSB-training | {language == "nl" ? "Hoe we het doen" : "How we do it"}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name="description" content={content.metaDescription} />
      </Head>
      <div className="chatandbanner-wrap  mg-bot-100">
        <Container maxWidth="md">
          <div className="chatandbanner-block">
            <div className="textblock">
              <div className="content mg-top-50">
                <Breadcrumbs />
                <div className="title">
                  <h1>
                    <FormattedMessage id={content.titleBlockTitle} />
                  </h1>
                </div>
                <div className="paragraph preface mg-bot-25">
                  <p>
                    <FormattedMessage id={content.titleBlockSubtitle} />
                  </p>
                </div>
                <div className="paragraph" style={{ opacity: 1 }}>
                  <p>
                    <FormattedMessage id="newsletter-para" />
                  </p>

                  <ul>
                    <li>
                      <FormattedMessage id="newsletter-list-1" />
                    </li>
                    <li>
                      <FormattedMessage id="newsletter-list-2" />
                    </li>
                    <li>
                      <FormattedMessage id="newsletter-list-3" />
                    </li>
                  </ul>

                  <form
                    method="POST"
                    action="https://behaviouralscienceforbusiness.activehosted.com/proc.php"
                    id="_form_3_"
                    className="_form _form_3 _inline-form  _dark"
                    noValidate
                    data-styles-version="3"
                  >
                    <input type="hidden" name="u" value="3" />
                    <input type="hidden" name="f" value="3" />
                    <input type="hidden" name="s" />
                    <input type="hidden" name="c" value="0" />
                    <input type="hidden" name="m" value="0" />
                    <input type="hidden" name="act" value="sub" />
                    <input type="hidden" name="v" value="2" />
                    <input type="hidden" name="or" value="9494dd3d71f50b896eaeb026a0b8350f" />
                    <div className="_form-content popup__formcontent">
                      <div className="_form_element _x07858933 _full_width pop__inputcontainer">
                        <label htmlFor="email" className="_form-label"></label>
                        <div className="_field-wrapper popup__inputwrapper" style={{ display: "flex", flexDirection: "column" }}>
                          <input type="text" id="email" name="email" placeholder="E-mail" required className="popup__input" />
                        </div>
                      </div>
                      <div className="_button-wrapper _full_width">
                        <button id="_form_3_submit" className="_submit popup__submit no-border" type="submit">
                          <FormattedMessage id="newsletter-button-text" />
                          <div className="popup__submit-icon">
                            <ChevronRightIcon fontSize="" />
                          </div>
                        </button>
                      </div>
                      <div className="_clear-element"></div>
                    </div>

                    {/* <div className='_form-thank-you' style='display:none;'></div> */}
                  </form>
                </div>
              </div>
            </div>
            <div className="media-wrap">
              <div className="newbanner dark-image">
                <Image
                  class="cirkel_howwedo"
                  src={`/images/20751_BSB_logo-cirkels copy 8.png`}
                  width={2000}
                  height={2000}
                  priority="true"
                />
                <Image
                  //   src={'https:' + content.topImage.fields.file.url}
                  src={image}
                  alt={content.topImageAlt}
                  layout="fill"
                  objectFit="cover"
                  objectPosition="center"
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className="eventsblock-wrap mg-bot-50">
        <Container maxWidth="md">
          <div className="textblock-wrap">
            <div className="cirkel_howwedo_wrap">
              <Image class="cirkel" src={`/images/20751_BSB_logo-cirkels_groen.png`} width={150} height={1300} priority="true" />
            </div>
            <div className="textblock">
              <div className="content">
                <div className="title">
                  <h2>{/* <FormattedMessage id={content.addedBlockTitle} /> */}</h2>
                </div>
                <div className="paragraph">
                  <p>{/* <FormattedMessage id={content.addedBlockParagraph} /> */}</p>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
