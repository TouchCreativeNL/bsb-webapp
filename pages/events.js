import Container from "@material-ui/core/Container";
import Image from "next/image";
import RedirectLink from "../components/redirect-link/RedirectLink";
import Breadcrumbs from "../components/breadcrumbs/Breadcrumbs";
import WithScrollBar from "../components/carousel/WithScrollBar";
import { FormattedMessage } from "react-intl";
import client from "../client";
import Head from "next/head";

export const getServerSideProps = async ({ params, locale }) => {
  let trackedEventsData = [];
  let eventsData = [];
  let dataEventsPage = [];

  const curLocale = locale == "nl" ? locale : "en-US";

  const { items } = await client.getEntries({
    content_type: "trackedEvents",
    content_type: "event",
    content_type: "eventsPage",
    locale: curLocale,
  });

  await client.getEntries({ content_type: "trackedEvents", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return trackedEventsData.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "event", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return eventsData.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "eventsPage", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataEventsPage.push(item.fields);
    });
  });

  return {
    props: {
      curLocale,
      dataEventsPage,
      dataEventsPage: items[0].fields,
      trackedEvent: items[0].fields,
      trackedEventsData,
      eventData: items[0].fields,
      eventsData,
    },
  };
};

const query = `query {
        trackedEventsCollection {
        items {
            subTitle,
            eventUrl
        }
        }
    }`;

export default function Home({ curlocale, dataEventsPage, trackedEvent, trackedEventsData, eventData, eventsData, Items }) {
  const language = curlocale;
  const content = dataEventsPage;
  return (
    <div className="content">
      <Head>
        {/* <title>
          BSB-training | {language == "nl" ? "Evenementen" : "Events"}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name="description" content={content.metaDescription} />
      </Head>
      <div className="chatandbanner-wrap  mg-bot-100">
        <Container maxWidth="md">
          <div className="chatandbanner-block">
            <div className="textblock">
              <div className="content mg-top-50">
                <Breadcrumbs />
                <div className="title">
                  <h1>
                    <FormattedMessage id={content.eventsTitle} />
                  </h1>
                </div>
                <div className="paragraph preface">
                  <p>
                    <FormattedMessage id={content.eventsParagraph} />
                  </p>
                </div>
              </div>
              <div className="media-wrap">
                <div className="newbanner">
                  <Image
                    src={`https:` + content.eventPageImage.fields.file.url}
                    alt={content.topImageAlt}
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center"
                  />
                </div>
              </div>
            </div>
          </div>
        </Container>
        <div className="our-events-wrap mg-bot-100">
          <Container maxWidth="md">
            <div className="title">
              <h1>
                <FormattedMessage id="ev-carou-1-title" />
              </h1>
            </div>
          </Container>
          <div className="events text-underneath">
            <WithScrollBar
              btnid={language == "nl" ? "Leer meer over deze training" : "Learn more about this training"}
              parentPath="events"
              data={eventsData}
            />
          </div>
        </div>
        {/* Removal of 'events we keep track of' For now commented */}
        {/* <div className="events-we-track-wrap mg-bot-100">
          <div className="cirkel_left_wrap">
            <Image class="cirkel" src={`/images/20751_BSB_logo-cirkels_groen.png`} width={150} height={1300} priority="true" />
          </div>
          <Container maxWidth="md">
            <div className="title">
              <h1>
                <FormattedMessage id="ev-carou-2-title" />
              </h1>
            </div>
          </Container>
          <div className="events text-underneath">
            <WithScrollBar btnid={language == "nl" ? "Bekijk dit evenement" : "View this event"} data={trackedEventsData} />
          </div>
        </div> */}
      </div>
    </div>
  );
}
