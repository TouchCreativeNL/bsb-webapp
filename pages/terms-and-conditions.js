import Head from 'next/head';
import Container from '@material-ui/core/Container';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';

export default function Home() {
  return (
    <>
      <Head>
        <title>BSB-training | Terms and conditions</title>
      </Head>
      <div className='content mg-top-50'>
        <div className='textblock-wrap mg-bot-50'>
          <Container maxWidth='md'>
            <div className='textblock'>
              <div className='content'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id='disc-title' />
                  </h1>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id='disc-para' />
                  </p>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>
    </>
  );
}
