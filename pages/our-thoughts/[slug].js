import Container from '@material-ui/core/Container';
import TrainingBlock from '../../components/trainingblock/TrainingBlock';
import Image from 'next/image';
import client from '../../client';
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import { INLINES, BLOCKS } from '@contentful/rich-text-types';
import { PropTypes } from '@material-ui/core';
import Head from 'next/head';

export const getServerSidePaths = async () => {
  const res = await client.getEntries({
    content_type: 'blogPost',
  });

  const paths = [];
  const locales = ['nl', 'en'];

  res.items.forEach(item => {
    locales.forEach(lang => {
      paths.push({ params: { slug: item.fields.slug }, locale: lang });
    });
  });

  return {
    paths,
    fallback: false,
  };
};

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == 'nl' ? locale : 'en-US';

  let dataBlogPosts = [];

  const { items } = await client.getEntries({
    content_type: 'blogPost',
    locale: curLocale,
    'fields.slug': params.slug,
  });

  await client.getEntries({ content_type: 'blogPost' }).then(entries => {
    const list = entries.items;
    list.map(item => {
      return dataBlogPosts.push(item.fields);
    });
  });

  return {
    props: { blogPost: items[0].fields, dataBlogPosts },
  };
};

const query = `query{
        blogPostCollection {
        items {
          linkTextRich {
            json
          }
        }
      } 
    }`;

export default function Home({ blogPost, dataBlogPosts, Items }) {
  const content = blogPost;
  const blogFields = dataBlogPosts.slice(1, 3);

  const firstTextBlockParagraph = {
    nodeType: 'document',
    data: {},
    content: [
      {
        nodeType: 'paragraph',
        data: {},
        content: [
          {
            nodeType: 'text',
            value: content.firstTextBlockParagraph,
            marks: [{ type: 'normal' }],
          },
        ],
      },
    ],
  };

  const lastTextBlockParagraph = {
    nodeType: 'document',
    data: {},
    content: [
      {
        nodeType: 'paragraph',
        data: {},
        content: [
          {
            nodeType: 'text',
            value: content.secondTextBlockParagraph,
            marks: [{ type: 'normal' }],
          },
        ],
      },
    ],
  };

  const options = {
    renderText: text =>
      text.split('\n').flatMap((text, i) => [i > 0 && <br />, text]),
  };

  const RICH_OPTIONS = {
    renderNode: {
      [INLINES.HYPERLINK]: (node, children) => {
        return (
          <a target='_blank' className='blog-link' href={node.data.uri}>
            {children}
          </a>
        );
      },
    },
  };

  return (
    <div className='content'>
      <Head>
        {/* <title>BSB-training | {content.headTitle}</title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content start'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id='blogs-title-1' />
                  </h1>
                </div>
                <div className='paragraph preface'>
                  {content.prefaceParagraph}
                </div>
                <div className='paragraph'>{content.paragraph}</div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner video dark-image'>
                <Image
                  src={'https:' + content.bannerImage.fields.file.url}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                  alt={content.bannerImageAlt}
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='textblock-wrap'>
        <div className='cirkel_speciality_wrap'>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels_groen.png`}
            width={150}
            height={1300}
            priority='true'
          />
        </div>
        <Container maxWidth='md'>
          <div className='textblock mg-bot-50 padding-right-150'>
            <div className='content start'>
              <div className='title'>
                <h2>{content.headTitle}</h2>
              </div>
              <div className='paragraph preface normal-font mg-bot-25'>
                {content.secondPrefaceParagraph &&
                  content.secondPrefaceParagraph}
              </div>
              <div className='paragraph'>
                {content.firstTextBlockParagraph &&
                  documentToReactComponents(firstTextBlockParagraph, options)}
              </div>
              {content.blogImage && (
                <div className='image'>
                  <Image
                    src={'https:' + content.blogImage.fields.file.url}
                    alt={content.blogImageAlt}
                    layout='fill'
                    objectFit='cover'
                    objectPosition='center'
                  />
                </div>
              )}
              <div className='paragraph'>
                {content.secondTextBlockParagraph &&
                  documentToReactComponents(lastTextBlockParagraph, options)}
              </div>
              <div className='paragraph link-para'>
                {content.linkTextRich &&
                  documentToReactComponents(content.linkTextRich, RICH_OPTIONS)}
              </div>
              <div className='author'>Written by {content.authorName}</div>
            </div>
          </div>
        </Container>
      </div>
      {/**        <div className="blog-posts-wrap mg-bot-100">
             <Container maxWidth="md">
             <div className="title">
             <h2>Related blogs</h2>
             </div>
             <div className="blog-posts">
             <TrainingBlock data={blogFields}/>
             </div>
             </Container>
             </div> */}
    </div>
  );
}
