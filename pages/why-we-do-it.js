import Container from "@material-ui/core/Container";
import Image from "next/image";
import RedirectLink from "../components/redirect-link/RedirectLink";
import Breadcrumbs from "../components/breadcrumbs/Breadcrumbs";
import { FormattedMessage } from "react-intl";
import Head from "next/head";
import client from "../client";
import { marked } from "marked";
import DOMPurify from "dompurify";
import { MarkdownToHtml } from "../components/markdown-to-html/markdownToHtml";

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == "nl" ? locale : "en-US";

  let dataWhyPage = [];

  const { items } = await client.getEntries({
    content_type: "waaromWijHetDoen",
    locale: curLocale,
  });

  // console.log("items: ", items);

  await client.getEntries({ content_type: "waaromWijHetDoen", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataWhyPage.push(item.fields);
    });
  });

  return {
    props: {
      dataWhyPage,
      dataWhyPage: items[0].fields,
      curLocale,
    },
  };
};

export default function Home({ dataWhyPage, curLocale }) {
  const language = curLocale;
  const content = dataWhyPage;
  const htmlContent = marked(content.whyQuestionParagraph);
  // const sanitizeHtmlContent = DOMPurify.sanitize(htmlContent);
  // const sanitizeHtmlContent = DOMPurify.sanitize(htmlContent);

  return (
    <div className="content">
      <Head>
        {/* <title>
          BSB-training |{" "}
          {language == "nl" ? "Waarom wij het doen" : "Why we do it"}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name="description" content={content.metaDescription} />
      </Head>
      <div className="chatandbanner-wrap  mg-bot-100">
        <Container maxWidth="md">
          <div className="chatandbanner-block">
            <div className="textblock">
              <div className="content start">
                <Breadcrumbs />
                <div className="title">
                  <h1>
                    <FormattedMessage id={content.whyTitle} />
                  </h1>
                </div>
                <div className="paragraph preface">
                  <FormattedMessage id={content.whyParagraph} />
                </div>
              </div>
            </div>
            <div className="media-wrap">
              <div className="newbanner">
                <Image
                  src={"https:" + content.whyPageImage.fields.file.url}
                  alt={content.whyPageImageAlt}
                  layout="fill"
                  objectFit="cover"
                  objectPosition="center"
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className="textblock-wrap">
        <Container maxWidth="md">
          <div className="textblock mg-bot-50">
            <div className="content start mg-top-50">
              <div className="title">
                <h2>
                  <FormattedMessage id={content.whyQuestionTitle} />
                </h2>
              </div>
              <div className="paragraph preface"></div>
              <div className="paragraph">
                {/* <FormattedMessage id={content.whyQuestionParagraph} /> */}
                <MarkdownToHtml content={content.whyQuestionParagraph} />
              </div>
              <div className="paragraph"></div>
            </div>
          </div>
        </Container>
      </div>

      <div className="socials-wrap mg-bot-100">
        <Container maxWidth="md">
          <div className="socials">
            <div className="title">
              <h2>
                <FormattedMessage id="want-to-know-more" />
              </h2>
            </div>
            <div className="social">
              <div className="image">
                <Image
                  src={"https:" + content.knowMoreImage.fields.file.url}
                  alt={content.knowMoreImageAlt}
                  layout="fill"
                  objectFit="cover"
                  objectPosition="center"
                />
              </div>
              <div className="content">
                <div className="title">
                  <h3>Julia Brouwers</h3>
                </div>
                <div className="paragraph">
                  <p></p>
                </div>
                <RedirectLink link="https://www.linkedin.com/in/julia-brouwers-4a494aa5/" text="LinkedIn" />
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
