import Head from 'next/head';
import Container from '@material-ui/core/Container';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';

export default function Home() {
  return (
    <>
      <Head>
        <title>BSB-training | Privacy</title>
      </Head>
      <div className='content mg-top-100'>
        <div className='textblock-wrap mg-bot-50'>
          <Container maxWidth='md'>
            <div className='textblock legal-documents'>
              <div className='content mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id='privacy-title' />
                  </h1>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id='privacy-text' />
                  </p>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>
    </>
  );
}
