import EventsBlock from '../components/eventsblock/Eventsblock';
import Container from '@material-ui/core/Container';
import Image from 'next/image';
import { data } from '../public/data/training-block';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';
import client from '../client';
import Head from 'next/head';

export const getServerSideProps = async ({ params, locale }) => {
  let dataTrainingPage = [];
  let dataTrainings = [];

  const curLocale = locale == 'nl' ? locale : 'en-US';

  const { items } = await client.getEntries({
    content_type: 'trainingPage',
    locale: curLocale,
  });

  await client
    .getEntries({ content_type: 'trainingPage', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataTrainingPage.push(item.fields);
      });
    });

  await client.getEntries({ content_type: 'training' }).then(entries => {
    const list = entries.items;
    list.map(item => {
      return dataTrainings.push(item.fields);
    });
  });

  return {
    props: {
      dataTrainingPage,
      dataTrainingPage: items[0].fields,
      dataTrainings,
      curLocale,
    },
  };
};

export default function Home({ dataTrainingPage, dataTrainings, curLocale }) {
  const language = curLocale;
  const trainingFields = dataTrainings;
  const content = dataTrainingPage;

  return (
    <div className='content'>
      <Head>
        {/* <title>BSB-training | Training</title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id={content.trainingBlockTitle} />
                  </h1>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id={content.trainingBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner dark-image'>
                <Image
                  src={'https:' + content.trainingPageImage.fields.file.url}
                  alt={content.topImageAltText}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='eventsblock-wrap mg-bot-50'>
        <Container maxWidth='xl'>
          <div className='eventsblock'>
            <EventsBlock
              data={trainingFields}
              buttontext={language == 'nl' ? 'Zie dit' : 'See this'}
              path='/trainings'
            />
          </div>
        </Container>
      </div>
    </div>
  );
}
