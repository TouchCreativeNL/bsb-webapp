import TrainingBlock from "../components/trainingblock/TrainingBlock";
import Container from "@material-ui/core/Container";
import Image from "next/image";
import { data } from "../public/data/this-us";
import RedirectLink from "../components/redirect-link/RedirectLink";
import Breadcrumbs from "../components/breadcrumbs/Breadcrumbs";
import { FormattedMessage } from "react-intl";
import Head from "next/head";
import client from "../client";

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == "nl" ? locale : "en-US";

  let dataUsPage = [];

  const { items } = await client.getEntries({
    content_type: "ditZijnWij",
    locale: curLocale,
  });

  await client.getEntries({ content_type: "ditZijnWij", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataUsPage.push(item.fields);
    });
  });

  return {
    props: {
      dataUsPage,
      dataUsPage: items[0].fields,
      curLocale,
    },
  };
};

export default function Home({ dataUsPage, curLocale }) {
  const language = curLocale;
  const content = dataUsPage;

  return (
    <div className="content">
      <Head>
        {/* <title>
          BSB-training | {language == "nl" ? "Dit zijn wij" : "This is us"}
        </title> */}
        <title>{content.pageTitle}</title>
        <meta name="description" content={content.metaDescription} />
      </Head>
      <div className="chatandbanner-wrap  mg-bot-100">
        <Container maxWidth="md">
          <div className="chatandbanner-block">
            <div className="textblock">
              <div className="content mg-top-50">
                <Breadcrumbs />
                <div className="title">
                  <h1>
                    <FormattedMessage id={content.usBlockTitle} />
                  </h1>
                </div>
                <div className="paragraph">
                  <p>
                    <FormattedMessage id={content.usBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
            <div className="media-wrap">
              <div className="newbanner">
                <Image
                  src={"https:" + content.thisisusImage.fields.file.url}
                  alt={content.thisIsUsImageAlt}
                  layout="responsive"
                  width={530}
                  height={690}
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className="this-us-wrap mg-bot-100">
        <div className="cirkel_left_wrap">
          <Image class="cirkel" src={`/images/20751_BSB_logo-cirkels_groen.png`} width={150} height={1300} priority="true" />
        </div>
        <Container maxWidth="md">
          <div className="this-us">
            <div className="member">
              <Image
                src={"https:" + content.juliaImage.fields.file.url}
                alt={content.juliaImageAlt}
                width="300"
                height="330"
                objectFit="cover"
                objectPosition="center"
              />
              <div className="content">
                <div className="title">
                  <h3>Julia Brouwers</h3>
                </div>
                <div className="paragraph">
                  <p>
                    {" "}
                    <FormattedMessage id={content.socialInformation1} />{" "}
                  </p>
                </div>
                <RedirectLink link="https://www.linkedin.com/in/julia-brouwers-4a494aa5/" text="LinkedIn" />
              </div>
            </div>
            <div className="member">
              <Image
                src={"https:" + content.duncanImage.fields.file.url}
                alt={content.duncanImageAlt}
                width="300"
                height="330"
                objectFit="cover"
                objectPosition="center"
              />
              <div className="content">
                <div className="title">
                  <h3>Duncan Rooders</h3>
                </div>
                <div className="paragraph">
                  <p>
                    {" "}
                    <FormattedMessage id={content.socialInformation2} />{" "}
                  </p>
                </div>
                <RedirectLink link="https://www.linkedin.com/in/duncan-rooders/" text="LinkedIn" />
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
