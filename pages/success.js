import Container from '@material-ui/core/Container';
import Image from 'next/image';
import RedirectLink from '../components/redirect-link/RedirectLink';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import { FormattedMessage } from 'react-intl';
import Head from 'next/head';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Button from '../components/button/Button';

export default function Home({ dataUsPage, curLocale }) {
  const language = curLocale;
  const content = dataUsPage;

  return (
    <div className='content'>
      <Head>
        {/* <title>
            BSB-training | {language == "nl" ? "Dit zijn wij" : "This is us"}
          </title> */}
        {/* <title>{content.pageTitle}</title> */}
        {/* <meta name='description' content={content.metaDescription} /> */}
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content mg-top-50'>
                {/* <Breadcrumbs /> */}
                <div className='title'>
                  <h1>
                    <FormattedMessage id='success-title' />
                  </h1>
                </div>
                <div className='paragraph'>
                  <p>
                    <FormattedMessage id='success-para' />
                  </p>
                </div>

                <Button id='back-to-home-button' link='/' target='_self' />

                {/* <button className='button no-border'>
                  Back to home{' '}
                  <div className='popup__submit-icon'>
                    <ChevronRightIcon fontSize='' />
                  </div>
                </button> */}
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner'>
                <Image
                  src='/images/success.jpeg'
                  //   alt={content.thisIsUsImageAlt}
                  layout='responsive'
                  width={530}
                  height={690}
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
