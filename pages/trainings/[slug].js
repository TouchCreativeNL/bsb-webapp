import Container from '@material-ui/core/Container';
import Image from 'next/image';
import Dynamic from 'next/dynamic';
import RedirectLink from '../../components/redirect-link/RedirectLink';
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs';
import client from '../../client';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import { BLOCKS, MARKS } from '@contentful/rich-text-types';
import { FormattedMessage } from 'react-intl';
import { Language } from '@material-ui/icons';
import Head from 'next/head';
import Button from '../../components/button/Button';

export const getServerSidePaths = async ({ locale }) => {
  const res = await client.getEntries({
    content_type: 'training',
  });

  let paths = [];
  const locales = ['en', 'nl'];

  res.items.forEach(item => {
    locales.forEach(lang => {
      paths.push({ params: { slug: item.fields.slug }, locale: lang });
    });
  });

  return {
    paths,
    fallback: false,
  };
};

export const getServerSideProps = async ({ params, locale }) => {
  let dataEvents = [];
  let dataTrainings = [];
  let dataBlogs = [];
  let dataSpecialties = [];

  const curLocale = locale == 'nl' ? locale : 'en-US';

  const { items } = await client.getEntries({
    content_type: 'training',
    locale: curLocale,
    'fields.slug': params.slug,
  });
  return {
    props: {
      training: items[0].fields,
      locale,
    },
  };
};

export default function Home({
  locale,
  dataEvents,
  dataTrainings,
  dataBlogs,
  dataSpecialties,
  Items,
  training,
}) {
  const content = training;

  const firstTextBlockParagraph = {
    nodeType: 'document',
    data: {},
    content: [
      {
        nodeType: 'paragraph',
        data: {},
        content: [
          {
            nodeType: 'text',
            value: content.firstTextBlockParagraph,
            marks: [{ type: 'normal' }],
          },
        ],
      },
    ],
  };
  const para = {
    nodeType: 'document',
    data: {},
    content: [
      {
        nodeType: 'paragraph',
        data: {},
        content: [
          {
            nodeType: 'text',
            value: content.para,
            marks: [{ type: 'normal' }],
          },
        ],
      },
    ],
  };
  const options = {
    renderText: text =>
      text.split('\n').flatMap((text, i) => [i > 0 && <br />, text]),
  };

  return (
    <div className='content'>
      <Head>
        {/* <title>BSB-training | {content.headTitle}</title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock start'>
              <div className='content mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>{content.headTitle}</h1>
                </div>
                <div className='paragraph preface'>
                  {documentToReactComponents(para, options)}
                </div>
                <div className='paragraph mg-bot-25'>
                  {content.secondPrefaceParagraph}
                </div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner video dark-image'>
                <Image
                  class='cirkel_speciality'
                  src={`/images/20751_BSB_logo-cirkels copy 8.png`}
                  width={2000}
                  height={2000}
                  priority='true'
                />
                <Image
                  src={'https:' + content.bannerImage.fields.file.url}
                  alt={content.topImageAlt}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='textblock-wrap'>
        <div className='cirkel_left_wrap'>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels_groen.png`}
            width={150}
            height={1300}
            priority='true'
          />
        </div>
        <Container maxWidth='md'>
          <div className='textblock mg-bot-50 padding-right-150'>
            <div className='content'>
              <div className='title'>
                <h2>{content.subTitle}</h2>
              </div>
              <div className='paragraph'>
                {documentToReactComponents(firstTextBlockParagraph, options)}
              </div>
              <div className='button-wrap'>
                <Button
                  id='enter-btn'
                  link={
                    locale == 'nl'
                      ? `mailto:info@bsb-training.com?subject=Stuur%20mij%20meer%20info%20over%20${content.headTitle}&body=Voornaam:%0AAchternaam:%0ABedrijf/organisatie:%0ATelefoonnummer%0AAantal%20deelnemers:%0AHoe%20kunnen%20we%20u%20helpen?`
                      : `mailto:info@bsb-training.com?subject=Please%20send%20me%20more%20info%20about%20${content.headTitle}&body=First%20name:%0ALast%20name:%0ACompany/organisation:%0APhone%20number:%0ANumber%20of%20participants:%0AHow%20can%20we%20help?`
                  }
                />
              </div>
              <div className='quote'>{content.quoteText}</div>
              <div className='paragraph'>
                {content.secondTextBlockParargraph}
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='video-wrap mg-bot-100'>
        <Container maxWidth='md'>
          <div className='video'>
            <iframe
              height='315'
              src='https://www.youtube.com/embed/7aK69PvQwfg?controls=0'
              title='YouTube video player'
              frameborder='0'
              allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
              allowfullscreen
            ></iframe>
          </div>
        </Container>
      </div>

      <div className='socials-wrap mg-bot-100'>
        <Container maxWidth='md'>
          <div className='socials'>
            <div className='title mg-bot-25'>
              <h2>
                <FormattedMessage id='training-know-more' />
              </h2>
            </div>
            <div className='social'>
              <div className='image'>
                <Image
                  src={'https:' + content.authorImage.fields.file.url}
                  alt={content.authorImageAlt}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
              <div className='content'>
                <div className='title for-whom-title'>
                  <h3>{content.forwhomtitle}</h3>
                </div>
                <div className='paragraph'>
                  <p>{content.forwhomtext}</p>
                </div>
                <div className='title for-whom-author'>
                  <h3>{content.authorTitle}</h3>
                </div>
                <div className='paragraph'>
                  <p>{content.authorParagraph}</p>
                </div>
                <RedirectLink text='LinkedIn' link={content.linkedinurl} />
              </div>
            </div>
          </div>
        </Container>
      </div>
      {/**
             *         <div className="reviews-wrap mg-bot-50">
                <Container maxWidth="md">
                    <div className="title">
                        <h2>
                            Reviews
                        </h2>
                    </div>
                </Container>
                <div className="reviews">
                    <Container maxWidth="md">
                        <div className="carousel">
                            <div className="review">
                                <div className="header">
                                    <div className="title">
                                        <h3>
                                            Fien Maarn
                                        </h3>
                                    </div>
                                    <div className="data">
                                        <div className="data">
                                            23 - 10 - 2020
                                        </div>
                                    </div>
                                </div>
                                <div className="content">
                                    <div className="paragraph">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="review">
                                <div className="header">
                                    <div className="title">
                                        <h3>
                                            Fien Maarn
                                        </h3>
                                    </div>
                                    <div className="data">
                                        23 - 10 - 2020
                                    </div>
                                </div>
                                <div className="content">
                                    <div className="paragraph">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
             *
             */}
    </div>
  );
}
