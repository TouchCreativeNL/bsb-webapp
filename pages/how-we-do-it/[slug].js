import Container from '@material-ui/core/Container';
import TrainingBlock from '../../components/trainingblock/TrainingBlock';
import Image from 'next/image';
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs';
import client from '../../client';

export const getStaticPaths = async () => {
  let blogList = [];

  const res = await client.getEntries({
    content_type: 'speciality',
  });

  const paths = res.items.map(item => {
    return {
      params: { slug: item.fields.slug },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }) => {
  let dataBlogPosts = [];

  const { items } = await client.getEntries({
    content_type: 'speciality',
    'fields.slug': params.slug,
  });

  await client.getEntries({ content_type: 'blogPost' }).then(entries => {
    const list = entries.items;
    list.map(item => {
      return dataBlogPosts.push(item.fields);
    });
  });

  return {
    props: { speciality: items[0].fields, dataBlogPosts },
  };
};

export default function Home({ speciality, dataBlogPosts }) {
  const content = speciality;
  const blogFields = dataBlogPosts.slice(1, 3);

  const data = [
    {
      title: 'Eerste blog post',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.',
      image: 'finance-img.jpg',
      link: '/trainingen/finance',
    },
    {
      title: 'Tweede blog post',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.',
      image: 'strategy-img.jpg',
      link: '/trainings/training',
    },
  ];

  return (
    <div className='content'>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content start'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>{content.headTitle}</h1>
                </div>
                <div className='paragraph preface'>
                  <p>{content.prefaceParagraph}</p>
                </div>
                <div className='paragraph'>
                  <p>{content.paragraph}</p>
                </div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner video dark-image'>
                <Image
                  class='cirkel_speciality'
                  src={`/images/20751_BSB_logo-cirkels copy 8.png`}
                  width={2000}
                  height={2000}
                  priority='true'
                />
                <Image
                  src={'https:' + content.bannerImage.fields.file.url}
                  alt='banner-image'
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='textblock-wrap'>
        <div class='cirkel_speciality_wrap'>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels_groen.png`}
            width={150}
            height={1300}
            priority='true'
          />
        </div>
        <Container maxWidth='md'>
          <div className='textblock mg-bot-50 padding-right-150'>
            <div className='content start mg-top-50'>
              <div className='title'>
                <h2>{content.subTitle}</h2>
              </div>
              <div className='paragraph preface mg-bot-25'>
                {content.secondPrefaceParagraph}
              </div>
              <div className='paragraph'>{content.firstTextBlockParagraph}</div>
            </div>
          </div>
        </Container>
      </div>
      <div className='video-wrap mg-bot-50'>
        <Container maxWidth='md'>
          <div className='video'>
            <iframe
              height='315'
              src='https://www.youtube.com/embed/rbsIImkq-_o?controls=0'
              title='YouTube video player'
              frameborder='0'
              allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
              allowfullscreen
            ></iframe>
          </div>
        </Container>
      </div>
      <div className='blog-posts-wrap mg-bot-100'>
        <Container maxWidth='md'>
          <div className='title'>
            <h2>Blog posts about {content.headTitle}</h2>
          </div>
          <div className='blog-posts'>
            <TrainingBlock data={blogFields} />
          </div>
        </Container>
      </div>
    </div>
  );
}
