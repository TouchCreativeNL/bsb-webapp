import '../styles/main.scss';
import { IntlProvider } from 'react-intl';
import { useRouter } from 'next/router';
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';
import Head from 'next/head';
import Script from 'next/script';
import client from '../client';
import { useEffect } from 'react';

const languages = {
  en: require('../locale/en.json'),
  nl: require('../locale/nl.json'),
};

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const { locale, defaultLocale } = router;
  const messages = languages[locale];

  useEffect(() => {
    async function fetchData() {
      const curLocale = router.locale === 'nl' ? router.locale : 'en-US';

      const { items } = await client.getEntries({
        content_type: 'general',
        locale: curLocale,
      });

      const metaDescription = items[0].fields.metaDescription;
      const metaKeywords = items[0].fields.metaKeywords;

      const description = document.querySelector('meta[name="description"]');
      const keywords = document.querySelector('meta[name="keywords"]');

      if (description) {
        description.setAttribute('content', metaDescription);
      }

      if (keywords) {
        keywords.setAttribute('content', metaKeywords);
      }
    }

    fetchData();
  }, [router.locale]);

  return (
    <IntlProvider
      messages={messages}
      locale={locale}
      defaultLocale={defaultLocale}
    >
      <Head>
        <script
          dangerouslySetInnerHTML={{
            __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KNQM5ML4');`,
          }}
        />
        <script src='/scripts/mailblue.js' />
        <link
          rel='icon'
          type='image/png'
          sizes='32x32'
          href='/favicon/android-chrome-192x192.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='16x16'
          href='/favicon/android-chrome-512x512.png'
        />
        <link
          rel='apple-touch-icon'
          sizes='180x180'
          href='/favicon/apple-touch-icon.png'
        />

        <link rel='manifest' href='/favicon/site.webmanifest' />
        <meta charSet='UTF-8' />
        <meta name='keywords' content='title, meta, nextjs' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <meta
          name='description'
          content='Wij passen behavioural science toe in uw team en organisatie. Wij onderzoeken, begrijpen, voorspellen en veranderen menselijk gedrag.
Wij verbeteren de kwaliteit van uw beslissingen, wij leren u over Strategie, Behavioural Finance en Human Dynamics. Wij transformeren menselijk gedrag in bedrijfsomgevingen.
'
        />
      </Head>
      <div className='container'></div>
      <Header />
      <noscript
        dangerouslySetInnerHTML={{
          __html: `<iframe src=https://www.googletagmanager.com/ns.html?id=GTM-KNQM5ML4
          height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
        }}
      />
      <Component {...pageProps} />
      <Footer />
    </IntlProvider>
  );
}

export default MyApp;
