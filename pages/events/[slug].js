import Container from '@material-ui/core/Container';
import TrainingBlock from '../../components/trainingblock/TrainingBlock';
import RedirectLink from '../../components/redirect-link/RedirectLink';
import Image from 'next/image';
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs';
import client from '../../client';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import { FormattedMessage } from 'react-intl';
import Head from 'next/head';
import Button from '../../components/button/Button';

export const getServerSidePaths = async ({}) => {
  const res = await client.getEntries({
    content_type: 'event',
  });

  const paths = [];
  const locales = ['nl', 'en'];

  res.items.forEach(item => {
    locales.forEach(lang => {
      paths.push({ params: { slug: item.fields.slug }, locale: lang });
    });
  });

  return {
    paths,
    fallback: false,
  };
};

export const getServerSideProps = async ({ params, locale }) => {
  const curLocale = locale == 'nl' ? locale : 'en-US';

  let dataEvents = [];

  const { items } = await client.getEntries({
    content_type: 'event',
    locale: curLocale,
    'fields.slug': params.slug,
  });

  await client.getEntries({ content_type: 'event' }).then(entries => {
    const list = entries.items;
    list.map(item => {
      return dataEvents.push(item.fields);
    });
  });

  return {
    props: { event: items[0].fields, dataEvents },
  };
};

export default function Home({ event, dataEvents }) {
  const content = event;
  const eventFields = dataEvents.slice(1, 3);

  const firstTextBlockParagraph = {
    nodeType: 'document',
    data: {},
    content: [
      {
        nodeType: 'paragraph',
        data: {},
        content: [
          {
            nodeType: 'text',
            value: content.firstTextBlockParagraph,
            marks: [{ type: 'normal' }],
          },
        ],
      },
    ],
  };

  const options = {
    renderText: text =>
      text.split('\n').flatMap((text, i) => [i > 0 && <br />, text]),
  };

  return (
    <div className='content'>
      <Head>
        {/* <title>BSB-training | {content.headTitle}</title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap  mg-bot-100'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='textblock'>
              <div className='content start mg-top-50'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>{content.headTitleDate}</h1>
                </div>
                <div className='paragraph preface'>
                  {content.prefaceParagraph}
                </div>
                <div className='paragraph'>{content.paragraph}</div>
              </div>
            </div>
            <div className='media-wrap'>
              <div className='newbanner video dark-image'>
                <Image
                  src={'https:' + content.bannerImage.fields.file.url}
                  alt={content.bannerImageAlt}
                  layout='fill'
                  objectFit='cover'
                  objectPosition='center'
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='textblock-wrap'>
        <div className='cirkel_left_wrap'>
          <Image
            class='cirkel'
            src={`/images/20751_BSB_logo-cirkels_groen.png`}
            width={150}
            height={1300}
            priority='true'
          />
        </div>
        <Container maxWidth='md'>
          <div className='textblock mg-bot-50 padding-right-150'>
            <div className='content start'>
              <div className='title'>
                <h2>{content.headTitle}</h2>
              </div>
              <div className='paragraph preface normal-font mg-bot-25'>
                {content.secondPrefaceParagraph}
              </div>
              <div className='paragraph'>
                {documentToReactComponents(firstTextBlockParagraph, options)}
              </div>
              <div className='button-wrap'>
                <Button id='enter-btn' link='mailto:info@bsb-training.com' />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className='speakers-wrap'>
        <Container maxWidth='md'>
          <div className='title'>
            <h3>
              <FormattedMessage id='speakers' />
            </h3>
          </div>
          <div className='speakers'>
            <div className='speakers mg-bot-50'>
              {content.speakersImage.map(item => {
                const speakers = [content.speakername1, content.speakername2];

                return (
                  <div className='speaker'>
                    <div className='speaker-image'>
                      <Image
                        src={'http:' + item.fields.file.url}
                        alt={item.fields.description}
                        layout='fill'
                        objectFit='cover'
                        objectPosition='center'
                      />
                    </div>
                    <p class='paragraph' style={{ fontWeight: '700' }}>
                      {item.fields.title}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </Container>
      </div>
      <div className='blog-posts-wrap mg-bot-100'>
        {/** <Container maxWidth="md">
                        <div className="title">
                            <h2>Gerelateerde <br/> evenementen</h2>
                        </div>
                        <div className="blog-posts">
                            <TrainingBlock data={eventFields}/>
                        </div>
                        <RedirectLink link="/events" id="readmore-abt-our-expertise"/>
                    </Container> */}
      </div>
    </div>
  );
}
