import Image from "next/image";
import Dynamic from "next/dynamic";
import client from "../client";

import TrainingBlock from "../components/trainingblock/TrainingBlock";
import EventsBlock from "../components/eventsblock/Eventsblock";
import Hero from "../components/hero/Hero";
import { data as firstBanner } from "../public/data/sector-banner";
import { data as secondBanner } from "../public/data/partner-banner";
import Button from "../components/button/Button";
import Banner from "../components/banner/Banner";
import Banner_2 from "../components/banner/Banner2";
import BannerWhitepaper from "../components/banner-whitepaper/BannerWhitepaper";
import Container from "@material-ui/core/Container";
import Head from "next/head";

const DynamicScrollbar = Dynamic(() => import("../components/carousel/WithScrollBar"));
import { FormattedMessage } from "react-intl";
import { Language } from "@material-ui/icons";

export const getServerSideProps = async ({ params, locale }) => {
  let dataHomepage = [];
  let dataEvents = [];
  let dataTrainings = [];
  let dataBlogs = [];
  let dataSpecialties = [];

  const curLocale = locale == "nl" ? locale : "en-US";

  const { items } = await client.getEntries({
    content_type: "event",
    content_type: "speciality",
    content_type: "training",
    content_type: "blogPost",
    content_type: "homePage",
    locale: curLocale,
  });

  await client.getEntries({ content_type: "homePage", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataHomepage.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "event", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataEvents.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "speciality", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataSpecialties.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "training", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataTrainings.push(item.fields);
    });
  });

  await client.getEntries({ content_type: "blogPost", locale: curLocale }).then((entries) => {
    const list = entries.items;
    list.map((item) => {
      return dataBlogs.push(item.fields);
    });
  });

  return {
    props: {
      dataHomepage,
      dataHomepage: items[0].fields,
      dataEvents,
      dataEvent: items[0].fields,
      dataTrainings,
      dataTraining: items[0].fields,
      dataBlogs,
      dataBlog: items[0].fields,
      dataSpecialties,
      dataSpecialty: items[0].fields,
      locale,
    },
  };
};

export default function Home({ locale, dataHomepage, dataEvents, dataTrainings, dataBlogs, dataSpecialties, Items }) {
  const content = dataHomepage;

  return (
    <div className="content">
      <Head>
        {/* <title>BSB-training | {locale == "nl" ? "Thuis" : "Home"}</title> */}
        <title>{content.pageTitle}</title>
        <meta name="description" content={content.metaDescription} />
        {/* <link rel='canonical' href={content.canonical}></link> */}
      </Head>
      <div className="hero-wrap mg-bot-25">
        <Hero text={content} />
      </div>
      {/*
            <div className="carousel-wrap mg-bot-50">
                <DynamicScrollbar data={specialityFields} buttonText="learn-more-btn"/>
            </div>
            */}
      <div class="cirkel-wrap">
        <Image class="cirkel" src={`/images/20751_BSB_logo-cirkels_groen.png`} width={150} height={1300} priority="true" />
      </div>
      <div className="textblock-wrap mg-bot-50">
        <Container maxWidth="md">
          <div className="textblock">
            <div className="content center width-75-precent ">
              <div className="title">
                <h2>
                  <FormattedMessage id={content.whyBlockTitle} />
                </h2>
              </div>

              <div className="paragraph">
                <p>
                  <FormattedMessage id={content.whyBlockParagraph} />
                </p>
              </div>

              <div className="button-wrap">
                <Button id="readmore-btn" link="/why-we-do-it" />
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className="training-wrap mg-bot-50">
        <Container maxWidth="md">
          <div className="textblock-wrap">
            <div className="textblock">
              <div className="content start">
                <div className="title">
                  <h2>
                    <FormattedMessage id={content.trainingBlockTitle} />
                  </h2>
                </div>

                <div className="paragraph">
                  <p>
                    <FormattedMessage id={content.trainingBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
          </div>
          <TrainingBlock data={dataTrainings} />
        </Container>
      </div>
      <div className="sector-wrap mg-bot-50">
        <h2 className="newbanner-title areas">
          <FormattedMessage id="focus-areas" />
        </h2>
        <Banner style="newbanner-1" data={firstBanner} background="home/Screenshot 2021-05-17 at 14.53.25.png" />
      </div>
      <div className="carousel-wrap mg-bot-50">
        <div className="textblock-wrap">
          <Container maxWidth="md">
            <div className="textblock">
              <div className="content center">
                <div className="title">
                  <h2>
                    <FormattedMessage id={content.ourThoughtsBlockTitle} />
                  </h2>
                </div>
                <div className="paragraph preface">
                  <p>
                    <FormattedMessage id={content.ourThoughtsBlockParagraph} />
                  </p>
                </div>
              </div>
            </div>
          </Container>
        </div>
        <DynamicScrollbar parentPath="our-thoughts" data={dataBlogs} buttonText={locale == "nl" ? "Ga door" : "Continue"} />
      </div>
      {/* WHITE PAPER DOWNLOAD */}
      {/* <div className='sector-wrap mg-bot-50'>
        <BannerWhitepaper
          style='newbanner-1'
          style2='icons_focus'
          data={firstBanner}
          title='Download whitepaper'
          background='finance-img.jpg'
          locale={locale}
        />
      </div> */}

      <div className="eventsblock-wrap mg-bot-100">
        <Container maxWidth="md">
          <div className="textblock-wrap">
            <div className="textblock start">
              <div className="content">
                <div className="title">
                  <h2>
                    <FormattedMessage id="home-ev-b-title" />
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <EventsBlock data={dataEvents} path="" text="readmore-abt-ev-btn" />
        </Container>
      </div>
      <div className="sector-wrap mg-bot-25">
        <h2 class="newbanner-title">
          <FormattedMessage id="home-member" />
        </h2>
        <h2 class="newbanner-title-second">
          <FormattedMessage id="home-associate" />
        </h2>
        <Banner_2 style="banner-2" data={secondBanner} title="" background="banner-2.jpg" />
      </div>
    </div>
  );
}
