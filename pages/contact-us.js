import { useState } from 'react';
import Container from '@material-ui/core/Container';
import Breadcrumbs from '../components/breadcrumbs/Breadcrumbs';
import Button from '../components/button/Button';
import { FormattedMessage } from 'react-intl';
import Head from 'next/head';
import client from '../client';

export async function getStaticProps({ locale }) {
  const curLocale = locale == 'nl' ? locale : 'en-US';

  let dataContactPage = [];

  const { items } = await client.getEntries({
    content_type: 'contact',
    locale: curLocale,
  });

  await client
    .getEntries({ content_type: 'contact', locale: curLocale })
    .then(entries => {
      const list = entries.items;
      list.map(item => {
        return dataContactPage.push(item.fields);
      });
    });

  return {
    props: {
      dataContactPage,
      dataContactPage: items[0].fields,
      curLocale,
    },
  };
}

export default function Home({ curLocale, dataContactPage }) {
  const language = curLocale;
  const content = dataContactPage;

  const [emailCreds, setEmailCreds] = useState({
    name: ' ',
    email: ' ',
    subject: ' ',
    message: ' ',
  });

  const notFilled = Object.values(emailCreds).find(i => i == ' ');
  return (
    <div className='content contact-us'>
      <Head>
        {/* <title>BSB-training | Contact</title> */}
        <title>{content.pageTitle}</title>
        <meta name='description' content={content.metaDescription} />
      </Head>
      <div className='chatandbanner-wrap'>
        <Container maxWidth='md'>
          <div className='chatandbanner-block'>
            <div className='contact-wrap textblock'>
              <div className='contact-form content'>
                <Breadcrumbs />
                <div className='title'>
                  <h1>
                    <FormattedMessage id='contact-us-title' />
                  </h1>
                </div>
                <div className='paragraph '>
                  <p>
                    <FormattedMessage id='contact-us-para' />
                  </p>
                </div>
                <div className='contact-form'>
                  <input
                    className='input-field'
                    type='text'
                    placeholder={language == 'nl' ? 'Naam' : 'Name'}
                    onChange={e =>
                      setEmailCreds({ ...emailCreds, name: e.target.value })
                    }
                  />
                  <input
                    className='input-field'
                    type='text'
                    placeholder='E-mail'
                    onChange={e =>
                      setEmailCreds({ ...emailCreds, email: e.target.value })
                    }
                  />
                  <input
                    className='input-field'
                    type='text'
                    placeholder={language == 'nl' ? 'Onderwerp' : 'Subject'}
                    onChange={e =>
                      setEmailCreds({ ...emailCreds, subject: e.target.value })
                    }
                  />
                  <textarea
                    className='input-field'
                    cols='30'
                    rows='10'
                    placeholder={language == 'nl' ? 'Bericht' : 'Message'}
                    onChange={e =>
                      setEmailCreds({ ...emailCreds, message: e.target.value })
                    }
                  />
                </div>
                <Button
                  disabled={notFilled}
                  text={language == 'nl' ? 'Verstuur' : 'Send'}
                  link={`mailto:info@bsb-training.com?subject=${emailCreds.subject}&body=${emailCreds.message}`}
                />
              </div>
            </div>
            <div className='media-wrap'>
              <div className='map newbanner'>
                <iframe
                  src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2454.5628914221043!2d5.12295231592754!3d52.03306027995306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6642a0905ba3f%3A0xe43d3eefcb1fa03a!2sHeemsteedseweg%2026b%2C%203992%20LS%20Houten!5e0!3m2!1snl!2snl!4v1641479731507!5m2!1snl!2snl'
                  allowfullscreen=''
                  loading='lazy'
                ></iframe>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}
