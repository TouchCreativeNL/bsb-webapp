# BSB-training

## Om te beginnen

Om te beginnen met dit project, volg de onderstaande stappen:

**1. Clone de repository of unzip de source code**

**2. Installeer de dependencies**

Zorg dat Node.js v16.17.1 en npm geïnstalleerd zijn op jouw machine. Daarna run:

```
npm install
```

**3. Run de development server**

```
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) in de browser om het resultaat te bekijken.

## Over het project

Het project is gebouwd in Next en gebruikt Server Side Rendering voor productie. Hosting is via Vercel, wat inhoudt dat wijzigingen automatisch worden doorgevoerd op het moment dat je pushed naar Bitbucket. Je kunt inloggen in Vercel om de build logs te bekijken tijdens het builden.

### CMS

Het project is gekoppeld aan een [Contentful CMS](https://app.contentful.com/). In dit CMS kunnen properties per pagina aangemaakt worden, die vervolgens kunnen worden opgehaald in dit project.
