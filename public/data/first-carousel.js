const data = [

    {
        "title": "research",
        "image": "research-img.jpg",
        "link": "/training",
        "linktext": "Learn more"

    },
    {
        "title": "training",
        "image": "training-img.jpg",
        "link": "/training",
        "linktext": "Learn more"
    },
    {
        "title": "consultancy",
        "image": "consultancy-img.jpg",
        "link": "/training",
        "linktext": "Learn more"
    },
    {
        "title": "insight",
        "image": "insight-img.jpg",
        "link": "/training",
        "linktext": "Learn more"
    },
    {
        "title": "insight",
        "image": "insight-img.jpg",
        "link": "/training",
        "linktext": "Learn more"
    },
    {
        "title": "consultancy",
        "image": "consultancy-img.jpg",
        "link": "/training",
        "linktext": "Learn more"
    },

]

export default data;