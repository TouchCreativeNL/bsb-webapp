export const data = [
    {
        "title": "Eerste event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "finance-img.jpg",
        "link": "trainingen/finance"
    },
    {
        "title": "Tweede event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "strategy-img.jpg",
        "link": "trainingen/strategy"
    },
    {
        "title": "Derde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "dynamics-img.jpg",
        "link": "trainingen/dynamics"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "decision-img.jpg",
        "link": "trainingen/decision"
    }
]