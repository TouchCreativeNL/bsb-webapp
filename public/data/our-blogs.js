export const data = [
  {
    title: "Eerste blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "finance-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Tweede blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "strategy-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Derde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "dynamics-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Vierde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "decision-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Vierde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "decision-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Vierde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "decision-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Vierde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "decision-img.jpg",
    link: "blogs/blog",
  },
  {
    title: "Vierde blog post",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
    image: "decision-img.jpg",
    link: "blogs/blog",
  },
];
