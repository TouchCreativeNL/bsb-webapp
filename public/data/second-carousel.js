export const data = [

    {
        "title": "Eerste blog",
        "image": "eerste-blog.jpg",
        "link": "blogs/blog",
        "linktext": "Lees deze blog"
    },
    {
        "title": "Tweede blog",
        "image": "tweede-blog.jpg",
        "link": "blogs/blog",
        "linktext": "Lees deze blog"
    },
    {
        "title": "Derde blog",
        "image": "derde-blog.jpg",
        "link": "blogs/blog",
        "linktext": "Lees deze blog"
    },
    {
        "title": "Vierde blog",
        "image": "vierde-blog.jpg",
        "link": "blogs/blog",
        "linktext": "Lees deze blog"
    }

]