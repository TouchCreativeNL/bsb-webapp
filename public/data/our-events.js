export const data = [
    {
        "title": "Eerste event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-elle-hughes-4258192.jpg",
        "link": "events/event"
    },
    {
        "title": "Tweede event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-fauxels-3183186.jpeg",
        "link": "events/event"
    },
    {
        "title": "Derde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-laker-5792648.jpg",
        "link": "events/event"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-pixabay-261909.jpeg",
        "link": "events/event"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-pixabay-261909.jpeg",
        "link": "events/event"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-pixabay-261909.jpeg",
        "link": "events/event"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-pixabay-261909.jpeg",
        "link": "events/event"
    },
    {
        "title": "Vierde event",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "events/pexels-pixabay-261909.jpeg",
        "link": "events/event"
    }
]
