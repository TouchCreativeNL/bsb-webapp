export const data = [
    {
        "title": "Finance",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "finance-img.jpg",
        "link": "trainingen/finance"
    },
    {
        "title": "Strategy",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis lobortis consectetur.",
        "image": "strategy-img.jpg",
        "link": "trainingen/strategy"
    }
]