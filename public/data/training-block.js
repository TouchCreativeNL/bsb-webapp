export const data = [
    {
        "titleId": "home-tr-1-block-title",
        "textId": "home-tr-1-block-paragraph",
        "image": "training/pexels-charles-parker-5845721.jpg",
        "link": "trainings/training",
        "linktext" : "view-a-case-btn"
    },
    {
        "titleId": "home-tr-2-block-title",
        "textId": "home-tr-2-block-paragraph",
        "image": "training/pexels-karolina-grabowska-4219041.jpg",
        "link": "trainings/training",
        "linktext" : "view-a-case-btn"
    },
    {
        "titleId": "home-tr-3-block-title",
        "textId": "home-tr-3-block-paragraph",
        "image": "training/pexels-fauxels-3183186.jpg",
        "link": "trainings/training",
        "linktext" : "view-a-case-btn"
    },
    {
        "titleId": "home-tr-4-block-title",
        "textId": "home-tr-4-block-paragraph",
        "image": "training/pexels-daniel-torobekov-5694121.jpg",
        "link": "trainings/training",
        "linktext" : "view-a-case-btn"
    }
]
