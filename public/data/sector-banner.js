export const data = [
  {
    image: "change-management.svg",
    title: "Change Management",
  },
  {
    image: "strategy.svg",
    title: "Strategy",
  },
  {
    image: "networking.svg",
    title: "Human Dynamics",
  },
  {
    image: "decision-making.svg",
    title: "Decision Making",
  },
];
